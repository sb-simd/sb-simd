#|
Copyright (c) 2005 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(defpackage :cpuid (:use :cl))
(in-package :cpuid)


(defvar +cpu-std-feat-fields+
  #(:fpu :vme :de :pse :tsc :k86-msr :pae :mce :cmpxchg8b :apic :res10 :sysenter/exit :mtrr :gpe :mca :cmov :pat :pse-36 :psn :clflush :res20 :ds :acpi :mmx :fxsave/rstor :sse :sse2 :self-snoop :htt :tm :res30 :pbe))
    
(defvar +cpu-std-feat-fields-ecx+
  #(:sse3 :res1 :res2 :monitor :ds-cpl :res5 :res6 :eist :tm2 :res9 :cid :res11 :res12 :cmpxchg16b :stpm :res14 :res15
    0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0))

;; low bits are mostly same as in std-feat, so skip them.. 
(defvar +cpu-ext-feat-fields+
  #(0 1 2 3 4 5 6 7 8 9 10 :syscall/ret 12 13 14 15 16 17 18 19 :nx 21 :mmx-ext :mmx 24 25 26 27 28 :longmode/em64t :3dnow-ext :3dnow))


(defvar *cpu-features* nil)
(defvar *cpu-vendor* nil) ;; eg authenticamd
(defvar *cpu-name* nil)	;; eg AMD Athlon XP 
(defvar *cpu-cache* nil)


(defun inspect-cpu ()

  (let ((array (make-array 8 :element-type '(unsigned-byte 16) :initial-element 0)) c d)

    ;; vendor

    (sb-sys:%primitive sb-vm::%read-cpu/x86 #x00000000 array)

    (let ((temp (make-array 12 :element-type '(unsigned-byte 8) :initial-element 0)))
      (flet ((load-word (i u v) (setf (aref temp (+ i 3)) (ash (aref array u) -8)
				      (aref temp (+ i 2)) (logand (aref array u) 255)
				      (aref temp (+ i 1)) (ash (aref array v) -8)
				      (aref temp (+ i 0)) (logand (aref array v) 255))))
      (loop for i from 0 below 12 by 4
	    for (u v) in '((2 3) (6 7) (4 5))
	    do (load-word i u v))

      (setq *cpu-vendor* (map 'string #'code-char temp))))
					 

    ;; std features
    (setq *cpu-features* nil)

    (sb-sys:%primitive sb-vm::%read-cpu/x86 #x00000001 array)

    (setq
;;     a (logior (ash (aref array 0) 16) (aref array 1))
;;     b (logior (ash (aref array 2) 16) (aref array 3))
     c (logior (ash (aref array 4) 16) (aref array 5))
     d (logior (ash (aref array 6) 16) (aref array 7)))

    (loop for i from 0 below 32 do
	  (if (= (ldb (byte 1 i) d) 1)
	      (push (aref +cpu-std-feat-fields+ i) *cpu-features*)))

    (loop for i from 0 below 32 do
	  (if (= (ldb (byte 1 i) c) 1)
	      (push (aref +cpu-std-feat-fields-ecx+ i) *cpu-features*))))
  )

(defun inspect-cpu-ext ()
  (let ((array (make-array 8 :element-type '(unsigned-byte 16) :initial-element 0)) 
	c d (max-ext-func 0))

    ;; determine max ext func
    (sb-sys:%primitive sb-vm::%read-cpu/x86 #x80000000 array)
    (setq max-ext-func (logior (ash (aref array 0) 16) (aref array 1)))

    ;; ext features (AMD/Intel)
    (if (>= max-ext-func #x80000001)
	(progn
	  (sb-sys:%primitive sb-vm::%read-cpu/x86 #x80000001 array)
	  (setq d (logior (ash (aref array 6) 16) (aref array 7)))

	  (loop for i from 0 below 32 do
		(let ((flag (ldb (byte 1 i) d))
		      (feat (aref +cpu-ext-feat-fields+ i)))
		  (if (and (= flag 1) (keywordp feat))
		      (pushnew feat *cpu-features*))))))

    ;; cpu name (AMD/Intel)
    (setq *cpu-name* nil)
    (if (>= max-ext-func #x80000004)
	(let ((cpuname #())
	      (temp (make-array 16 :element-type '(unsigned-byte 8) :initial-element 0)))
	  (flet ((load-word (i u v) (setf (aref temp (+ i 3)) (ash (aref array u) -8)
					  (aref temp (+ i 2)) (logand (aref array u) 255)
					  (aref temp (+ i 1)) (ash (aref array v) -8)
					  (aref temp (+ i 0)) (logand (aref array v) 255))))
	    (flet ((conc-word ()
		     (loop for i from 0 below 16 by 4
			   for (u v) in '((0 1) (2 3) (4 5) (6 7))
			   do
			   (load-word i u v))
		     (setq cpuname (concatenate '(simple-array (unsigned-byte 8) (*)) cpuname temp))))

	      (sb-sys:%primitive sb-vm::%read-cpu/x86 #x80000002 array) (conc-word)
	      (sb-sys:%primitive sb-vm::%read-cpu/x86 #x80000003 array) (conc-word)
	      (sb-sys:%primitive sb-vm::%read-cpu/x86 #x80000004 array) (conc-word)
	      ))

	  ;; cut to null
	  (if (position 0 cpuname)
	      (setq cpuname (subseq cpuname 0 (position 0 cpuname))))
	  ;; coerce to string
	  (setq cpuname (map 'string #'code-char cpuname))
	  (setq *cpu-name* cpuname)))

    ;; cache (AMD)
    (setq *cpu-cache* nil)
    (if (>= max-ext-func #x80000005)
	(progn
	  (sb-sys:%primitive sb-vm::%read-cpu/x86 #x80000005 array)
	  (setq c (logior (ash (aref array 4) 16) (aref array 5))
		d (logior (ash (aref array 6) 16) (aref array 7)))

	  (push (list :l1-data :size (* 1024 (ldb (byte 8 24) c))) *cpu-cache*)
	  (push (list :l1-inst :size (* 1024 (ldb (byte 8 24) d))) *cpu-cache*)))


    (if (>= max-ext-func #x80000006)
	(progn
	  (sb-sys:%primitive sb-vm::%read-cpu/x86 #x80000006 array)
	  (setq c (logior (ash (aref array 4) 16) (aref array 5)))
	  (push (list :l2 :size (* 1024 (ldb (byte 16 16) c))) *cpu-cache*)))

    t))

(defun dump-cpu ()

    ;; dump

    (format t "~&")
    (format t "Vendor: ~A.~%" *cpu-vendor*)
    (format t "Features: ~S.~%" *cpu-features*)
    (format t "cpu: ~A, cache: ~S.~%" *cpu-name* *cpu-cache*)
    )
