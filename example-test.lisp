(in-package :cl-user)

(defun test-foo ()
  (let ((arr1 (make-array 10 :element-type 'single-float :initial-element 0f0))
	(arr2 (make-array 10 :element-type 'single-float :initial-element 0f0)))

    (loop for i from 0 below 10 
	  do (setf 
	      (aref arr1 i) (float (* i 100)) 
	      (aref arr2 i) (float i)))

    (format t "Before: ~S~%~S~%" arr1 arr2)
    (format t "b <- a + b, idx 0~%")

    (sb-sys:%primitive sb-vm::%sse-add/simple-array-single-float-1 arr2 arr2 arr1 0)

    (format t "After: ~S~%~S~%" arr1 arr2)

    (format t "a <- sqrt(b), idx 4~%")

    (sb-sys:%primitive sb-vm::%sse-sqrt/simple-array-single-float-1 arr1 arr2 4)

    (format t "After: ~S~%~S~%" arr1 arr2)

    ))

(defun test-2 ()
  (let ((arr1 (make-array 16 :element-type '(unsigned-byte 8) :initial-element 0))
	(arr2 (make-array 16 :element-type '(unsigned-byte 8) :initial-element 0)))
    
    (loop for i from 0 below 16 do (setf (aref arr1 i) (* (1+ i) 10)
					 (aref arr2 i) (1+ i)))

    (format t "Before: ~S~%~S~%" arr1 arr2)
    (format t "b <- a+b, idx 4~%")

    (sb-sys:%primitive sb-vm::%sse-add/simple-array-unsigned-byte-8-1 arr2 arr1 arr2 4)

    (format t "After: ~S~%~S~%" arr1 arr2)
    
    ))

(defparameter +sse-highbit-single-float-mask+ (make-array 16 :element-type '(unsigned-byte 8) 
						       :initial-contents '(0 0 0 128 
									   0 0 0 128
									   0 0 0 128
									   0 0 0 128)))
(defparameter +sse-lowbits-single-float-mask+ (make-array 16 :element-type '(unsigned-byte 8) 
						       :initial-contents '(255 255 255 127
									   255 255 255 127
									   255 255 255 127
									   255 255 255 127)))

(defun sign (float-array)
  (let ((res (make-array 16 :element-type '(unsigned-byte 8) :initial-element 0)))

    (sb-sys:%primitive sb-vm::%SSE-AND/SIMPLE-ARRAY-SINGLE-FLOAT/SIMPLE-ARRAY-UNSIGNED-BYTE-8-1 
		       res 
		       float-array 
		       +sse-highbit-single-float-mask+ 
		       0)
    (values-list (mapcar #'(lambda (x) (/= x 0)) (list (aref res 3) (aref res 7) (aref res 11) (aref res 15))))))

(defun %neg (float-array)
  (let ((res (make-array 4 :element-type 'single-float :initial-element 0f0)))

    (sb-sys:%primitive sb-vm::%SSE-XOR/SIMPLE-ARRAY-SINGLE-FLOAT/SIMPLE-ARRAY-UNSIGNED-BYTE-8-1 
		       res 
		       float-array 
		       +sse-highbit-single-float-mask+ 
		       0)
    res))

(defun %abs (float-array)
  (let ((res (make-array 4 :element-type 'single-float :initial-element 0f0)))

    (sb-sys:%primitive sb-vm::%SSE-AND/SIMPLE-ARRAY-SINGLE-FLOAT/SIMPLE-ARRAY-UNSIGNED-BYTE-8-1 
		       res 
		       float-array 
		       +sse-lowbits-single-float-mask+ 
		       0)
    res))

(defun test-sign ()
  (let ((arr1 (make-array 10 :element-type 'single-float :initial-element 0f0)))
    (loop for i from 0 below 10 do (setf (aref arr1 i) 
					 (float (* (expt -1 i) (- (* (1+ i) 10) (* 2 i i))))))
    (format t "array: ~S~%" arr1)
    (multiple-value-bind (s1 s2 s3 s4) (sign arr1)
      (format t "sign0->3: ~A ~A ~A ~A~%" s1 s2 s3 s4))
    (format t "neg: ~S~%" (%neg arr1))
    (format t "abs: ~S~%" (%abs arr1))
    t))