#|
Copyright (c) 2005 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(in-package :sb-vm)

(defmacro vect-ea (base &optional idx) 
  (let ((disp
	 (if (and idx (numberp idx))
	     `(+ (- (* VECTOR-DATA-OFFSET N-WORD-BYTES) OTHER-POINTER-LOWTAG) ,idx)
	     `(- (* VECTOR-DATA-OFFSET N-WORD-BYTES) OTHER-POINTER-LOWTAG))))

    (if (and idx (symbolp idx))
	`(make-ea :dword :base ,base :index ,idx :disp ,disp)
	`(make-ea :dword :base ,base :disp ,disp))))

(DEFINE-VOP (%sse-vect-add/single-float)
    (:POLICY :FAST-SAFE)
  (:ARGS (DEST :SCS (DESCRIPTOR-REG))
	 (SRC1 :SCS (DESCRIPTOR-REG))
	 (SRC2 :SCS (DESCRIPTOR-REG)))
  (:ARG-TYPES SIMPLE-ARRAY-SINGLE-FLOAT
	      SIMPLE-ARRAY-SINGLE-FLOAT
	      SIMPLE-ARRAY-SINGLE-FLOAT)

  (:TEMPORARY (:SC XMM-REG) X0)
  (:TEMPORARY (:SC XMM-REG) X1)

  (:GENERATOR 10
		(inst movdqu x0 (vect-ea src1))
		(inst movdqu x1 (vect-ea src2))
		(inst addps x0 x1)
		(inst movdqu (vect-ea dest) x0)))

(DEFINE-VOP (%sse-vect-add2/single-float)
    (:POLICY :FAST-SAFE)
  (:ARGS (SRC1 :SCS (XMM-REG))
	 (SRC2 :SCS (XMM-REG)))
  (:ARG-TYPES XMM XMM)

  (:RESULTS (DEST :SCS (XMM-REG)))

  (:TEMPORARY (:SC XMM-REG :from :argument :to :result) X0)
  (:TEMPORARY (:SC XMM-REG :from :argument) X1)

  (:GENERATOR 10
	      (move x0 src1)
	      (move x1 src2)
	      (inst addps x0 x1)
	      (move dest x0)
	      ))

(DEFINE-VOP (%sse-vect-sub/single-float)
    (:POLICY :FAST-SAFE)
  (:ARGS (DEST :SCS (DESCRIPTOR-REG))
	 (SRC1 :SCS (DESCRIPTOR-REG))
	 (SRC2 :SCS (DESCRIPTOR-REG)))
  (:ARG-TYPES SIMPLE-ARRAY-SINGLE-FLOAT
	      SIMPLE-ARRAY-SINGLE-FLOAT
	      SIMPLE-ARRAY-SINGLE-FLOAT)

  (:TEMPORARY (:SC XMM-REG) X0)
  (:TEMPORARY (:SC XMM-REG) X1)

  (:GENERATOR 10
		(inst movdqu x0 (vect-ea src1))
		(inst movdqu x1 (vect-ea src2))
		(inst subps x0 x1)
		(inst movdqu (vect-ea dest) x0)))

(DEFINE-VOP (%sse-vect-len/single-float)
    (:POLICY :FAST-SAFE)
  (:ARGS (DEST :SCS (DESCRIPTOR-REG))
	 (SRC1 :SCS (DESCRIPTOR-REG)))
  (:ARG-TYPES SIMPLE-ARRAY-SINGLE-FLOAT SIMPLE-ARRAY-SINGLE-FLOAT)

  (:TEMPORARY (:SC XMM-REG) X0)
  (:TEMPORARY (:SC XMM-REG) X1)

  (:GENERATOR 10
		(inst xorps x0 x0)
		(inst movdqu x1 (vect-ea src1))
		(inst mulps x1 x1) ;; ^2

		(inst movdqa x0 x1) ;; + 

		(inst psrldq-ib x1 4) ;; >> 4
		(inst addss x0 x1) ;; + 

		(inst psrldq-ib x1 4) ;; .. 
		(inst addss x0 x1)

		(inst psrldq-ib x1 4)
		(inst addss x0 x1) ;; here we have added up all single-floats

		(inst sqrtss x1 x0) ;; sqrt
		
		(inst movss (vect-ea dest) x1) ;; store scalar single-float
		))

(DEFINE-VOP (%sse-vect-scalar-mul/single-float)
    (:POLICY :FAST-SAFE)
  (:ARGS (DEST :SCS (DESCRIPTOR-REG))
	 (SRC1 :SCS (DESCRIPTOR-REG))
	 (SCALAR :SCS (DESCRIPTOR-REG)))
  (:ARG-TYPES SIMPLE-ARRAY-SINGLE-FLOAT SIMPLE-ARRAY-SINGLE-FLOAT SIMPLE-ARRAY-SINGLE-FLOAT)

  (:TEMPORARY (:SC XMM-REG) X0)
  (:TEMPORARY (:SC XMM-REG) X1)
  (:TEMPORARY (:SC XMM-REG) X2)

  (:GENERATOR 10
		(inst xorps x2 x2)
		(inst movdqu x0 (vect-ea src1))
		(inst movss x1 (vect-ea scalar))

		;; load scalar to all slots
		(inst addss x2 x1)
		(inst pslldq-ib x1 4)
		(inst orps x2 x1)
		(inst pslldq-ib x1 4)
		(inst orps x2 x1)
		(inst pslldq-ib x1 4)
		(inst orps x2 x1)

		;; mul vector with scalar-vector
		(inst mulps x0 x2)

		;; store
		(inst movdqu (vect-ea dest) x0) 
		))

(DEFINE-VOP (%sse-vect-normalize/single-float)
    (:POLICY :FAST-SAFE)
  (:ARGS (DEST :SCS (DESCRIPTOR-REG))
	 (SRC1 :SCS (DESCRIPTOR-REG)))
  (:ARG-TYPES SIMPLE-ARRAY-SINGLE-FLOAT SIMPLE-ARRAY-SINGLE-FLOAT)

  (:TEMPORARY (:SC XMM-REG) X0)
  (:TEMPORARY (:SC XMM-REG) X1)
  (:TEMPORARY (:SC XMM-REG) X2)

  (:GENERATOR 10
		(inst xorps x0 x0)
		(inst movdqu x1 (vect-ea src1))
		(inst movdqa x2 x1)

		;; calculate x0 <- 1 / sqrt( x^2 + y^2 + z^2 + w^2 )
		(inst mulps x1 x1) ;; ^2

		;; copy x1 to x0, then rotate/add
		(inst movdqa x0 x1) 

		(inst shufps x1 x1 #b10010011) ;; rotate
		(inst addps x0 x1) ;; + 

		(inst shufps x1 x1 #b10010011) ;; rotate
		(inst addps x0 x1) ;; + 

		(inst shufps x1 x1 #b10010011) ;; rotate
		(inst addps x0 x1) ;; + 

		(inst rsqrtps x1 x0) ;; 1 / sqrt

		(inst mulps x2 x1) ;; vect = vect * (1 / sqrt(len))

		(inst movdqu (vect-ea dest) x2) ;; store normalized vector
		))

(DEFINE-VOP (%sse-vect-dot/single-float)
    (:POLICY :FAST-SAFE)
  (:ARGS (DEST :SCS (DESCRIPTOR-REG))
	 (SRC1 :SCS (DESCRIPTOR-REG))
	 (SRC2 :SCS (DESCRIPTOR-REG)))

  (:ARG-TYPES
   SIMPLE-ARRAY-SINGLE-FLOAT
   SIMPLE-ARRAY-SINGLE-FLOAT
   SIMPLE-ARRAY-SINGLE-FLOAT)

  (:TEMPORARY (:SC XMM-REG) X0)
  (:TEMPORARY (:SC XMM-REG) X1)

  (:GENERATOR 10
		(inst movdqu x0 (vect-ea src1))
		(inst movdqu x1 (vect-ea src2))

		(inst mulps x1 x0) ;; a_n * b_n

		(inst movdqa x0 x1) ;;

		(inst psrldq-ib x1 4) ;; >> 4
		(inst addss x0 x1) ;; + 

		(inst psrldq-ib x1 4) ;; .. 
		(inst addss x0 x1)

		(inst psrldq-ib x1 4)
		(inst addss x0 x1) ;; here we have added up all single-floats

		(inst movss (vect-ea dest) x0) ;; store scalar single-float
		))

