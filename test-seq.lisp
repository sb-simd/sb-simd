(in-package :cl-user)

(declaim (optimize (speed 3) (safety 0) (space 0) (debug 0)))

(defun sse-seq= (seq1 seq2)
  (= (sb-sys:%primitive sb-vm::%sse-seq= seq1 seq2) 0))

(defun seq= (seq1 seq2)
  (declare (type (simple-array (unsigned-byte 8) (*)) seq1 seq2))
  (and (= (length seq1) (length seq2))
       (loop for equal = t
	     for s1 of-type unsigned-byte across seq1
	     for s2 of-type unsigned-byte across seq2
	     when (/= s1 s2) do (setq equal nil) (return nil)
	     finally (return equal))))


(defun test-seq (&optional (test-count 50000))
  (let ((arr1 (make-array #.(* 255 1025) :element-type '(unsigned-byte 8) :initial-element 0))
	(arr2 (make-array #.(* 255 1025) :element-type '(unsigned-byte 8) :initial-element 0))
	(arr3 (make-array #.(* 255 1025) :element-type '(unsigned-byte 8) :initial-element 0))
	(arr4 (make-array #.(* 255 1025) :element-type '(unsigned-byte 8) :initial-element 0))
	res)
    
    (loop for i from 0 below (length arr1) 
	  do (setf (aref arr1 i) (mod (* (1+ i) 10) 256)
		   (aref arr2 i) (aref arr1 i)
		   (aref arr3 i) (aref arr1 i)
		   (aref arr4 i) (aref arr1 i)
		   ))

    (setf (aref arr3 1200) (mod (1+ (aref arr3 1200)) 256)
	  (aref arr4 (- (length arr4) 2)) (mod (1+ (aref arr4 (- (length arr4) 2))) 256))

;;    (time (dotimes (i 100000) (sse-seq= arr1 arr2)))
;;    (time (dotimes (i #.(/ 100000 30)) (seq= arr1 arr2)))

    (format t "; seq= a1 a2~%")
    (time-sample-form #'(lambda () (dotimes (i (truncate test-count 15)) (setf res (seq= arr1 arr2)))))

    (format t "; seq= a1 a3~%")
    (time-sample-form #'(lambda () (dotimes (i (truncate test-count 15)) (setf res (seq= arr1 arr3)))))

    (format t "; seq= a2 a4~%")
    (time-sample-form #'(lambda () (dotimes (i (truncate test-count 15)) (setf res (seq= arr2 arr4)))))


    (format t "; sse-seq= a1 a2~%")
    (time-sample-form #'(lambda () (dotimes (i test-count) (setf res (sse-seq= arr1 arr2)))))

    (format t "; sse-seq= a1 a3~%")
    (time-sample-form #'(lambda () (dotimes (i test-count) (setf res (sse-seq= arr1 arr3)))))

    (format t "; sse-seq= a2 a4~%")
    (time-sample-form #'(lambda () (dotimes (i test-count) (setf res (sse-seq= arr2 arr4)))))

    ))
