;;; -*-  Lisp -*-

(in-package #:asdf)

(defsystem sb-simd
	:version "cvs"
	:components ((:file "detect-simd")
		     (:file "sse-vops" :depends-on ("detect-simd"))
		     (:file "sse-matrix" :depends-on ("sse-vops")))
	)

(defmethod perform :after ((o load-op (c (eql (find-system :sb-simd)))))
  (provide 'sb-simd))



