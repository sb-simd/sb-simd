#|
Copyright (c) 2005 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
#|

instruction reference:

http://www.amd.com/us-en/assets/content_type/white_papers_and_tech_docs/26568.pdf


TODO:

FXRSTOR. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 121
FXSAVE . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 124

LDMXCSR . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 140

MOVDQ2Q . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 178

MOVQ2DQ . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 208

STMXCSR . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 410


|#

(declaim (optimize (debug 3)))

(defun emit-ops (ops)
  (loop for op in ops 
	collect `(emit-byte segment ,op) into result
	finally (return result)))


(defun gen-ops (&optional (stream t))

  ;;; instructions like:
  ;;; ADDPS xmm1, xmm2/mem128 0F 58 /r
  (loop for (inst . ops) in 
	'(
	  ;; single precision float
	  (addps #x0F #x58)
	  (addsubps #xF2 #x0F #xD0)
	  (andnps #x0F #x55)
	  (andps #x0F #x54)
	  (divps #x0F #x5E)
	  (haddps #xF2 #x0F #x7C)
	  (hsubps #xF2 #x0F #x7D)
	  (maxps #x0F #x5F)
	  (minps #x0F #x5D)
	  (mulps #x0F #x59)
	  (orps #x0F #x56)
	  (rcpps #x0F #x53)
	  (rsqrtps #x0F #x52)
	  (sqrtps #x0F #x51)
	  (subps #x0F #x5C)
	  (unpckhps #x0F #x15)
	  (unpcklps #x0F #x14)
	  (xorps #x0F #x57)

	  ;; double precision float
	  (addpd #x66 #x0F #x58)
	  (addsubpd #x66 #x0F #xD0)
	  (andnpd #x66 #x0F #x55)
	  (andpd #x66 #x0F #x54)
	  (divpd #x66 #x0F #x5E)
	  (haddpd #x66 #x0F #x7C)
	  (hsubpd #x66 #x0F #x7D)
	  (maxpd #x66 #x0F #x5F)
	  (minpd #x66 #x0F #x5D)
	  (mulpd #x66 #x0F #x59)
	  (orpd #x66 #x0F #x56)
	  (sqrtpd #x66 #x0F #x51)
	  (subpd #x66 #x0F #x5C)
	  (unpckhpd #x66 #x0F #x15)
	  (unpcklpd #x66 #x0F #x14)
	  (xorpd #x66 #x0F #x57)

	  ;; scalar double precision float
	  (addsd #xF2 #x0F #x58)
	  (comisd #x66 #x0F #x2F)
	  (divsd #xF2 #x0F #x5E)
	  (maxsd #xF2 #x0F #x5F)
	  (minsd #xF2 #x0F #x5D)
	  (mulsd #xF2 #x0F #x59)
	  (sqrtsd #xF2 #x0F #x51)
	  (subsd #xF2 #x0F #x5C)
	  (ucomisd #x66 #x0F #x2E)

	  ;; scalar single precision float
	  (addss #xF3 #x0F #x58)
	  (comiss #x0F #x2F)
	  (divss #xF3 #x0F #x5E)
	  (maxss #xF3 #x0F #x5F)
	  (minss #xF3 #x0F #x5D)
	  (mulss #xF3 #x0F #x59)
	  (rcpss #xF3 #x0F #x53)
	  (rsqrtss #xF3 #x0F #x52)
	  (sqrtss #xF3 #x0F #x51)
	  (subss #xF3 #x0F #x5C)
	  (ucomiss #x0F #x2E)
	  

	  ;; packed integer
	  (packssdw #x66 #x0F #x6B)
	  (packsswb #x66 #x0F #x63)
	  (packuswb #x66 #x0F #x67)

	  (paddb #x66 #x0F #xFC)
	  (paddd #x66 #x0F #xFE)
	  (paddq #x66 #x0F #xD4)
	  (paddsb #x66 #x0F #xEC)
	  (paddsw #x66 #x0F #xED)
	  (paddusb #x66 #x0F #xDC)
	  (paddusw #x66 #x0F #xDD)
	  (paddw #x66 #x0F #xFD)

	  (pand #x66 #x0F #xDB)
	  (pandn #x66 #x0F #xDF)
	  
	  (pavgb #x66 #x0F #xE0)
	  (pavgw #x66 #x0F #xE3)
	  
	  (pcmpeqb #x66 #x0F #x74)
	  (pcmpeqd #x66 #x0F #x76)
	  (pcmpeqw #x66 #x0F #x75)
	  (pcmpgtb #x66 #x0F #x64)
	  (pcmpgtd #x66 #x0F #x66)
	  (pcmpgtw #x66 #x0F #x65)
	  
	  (pmaddwd #x66 #x0F #xF5)

	  (pmaxsw #x66 #x0F #xEE)
	  (pmaxub #x66 #x0F #xDE)
	  
	  (pminsw #x66 #x0F #xEA)
	  (pminub #x66 #x0F #xDA)

	  (pmovmskb #x66 #x0F #xD7)

	  (pmulhuw #x66 #x0F #xE4)
	  (pmulhw #x66 #x0F #xE5)
	  (pmullw #x66 #x0F #xD5)
	  (pmuludq #x66 #x0F #xF4)
	  
	  (por #x66 #x0F #xEB)
	  
	  (psadbw #x66 #x0F #xF6)
	  (pssld #x66 #x0F #xF2)
	  (psllq #x66 #x0F #xF3)
	  (psllw #x66 #x0F #xF1)
	  (psrad #x66 #x0F #xE2)
	  (psraw #x66 #x0F #xE2)
	  (psrld #x66 #x0F #xD2)
	  (psrlq #x66 #x0F #xD3)
	  (psrlw #x66 #x0F #xD1)
	  
	  (psubb #x66 #x0F #xF8)
	  (psubd #x66 #x0F #xFA)
	  (psubq #x66 #x0F #xFB)
	  (psubsb #x66 #x0F #xE8)
	  (psubsw #x66 #x0F #xE9)
	  (psubusb #x66 #x0F #xD8)
	  (psubusw #x66 #x0F #xD9)
	  (psubw #x66 #x0F #xF9)
	  
	  (punpckhbw #x66 #x0F #x68)
	  (punpckhdq #x66 #x0F #x6A)
	  (punpckhqdq #x66 #x0F #x6D)
	  (punpckhwd #x66 #x0F #x69)
	  (punpcklbw #x66 #x0F #x60)
	  (punpckldq #x66 #x0F #x62)
	  (punpcklqdq #x66 #x0F #x6C)
	  (punpcklwd #x66 #x0F #x61)
	  
	  (pxor #x66 #x0F #xEF)

	  ;; convert
	  (cvtdq2pd #xF3 #x0F #xE6)
	  (cvtdq2ps #x0F #x5B)
	  (cvtpd2dq #xF2 #x0F #xE6)
	  (cvtpd2pi #x66 #x0F #x2D)
	  (cvtpd2ps #x66 #x0F #x5A)
	  (cvtpi2pd #x66 #x0F #x2A)
	  (cvtpi2ps #x0F #x2A)
	  (cvtps2dq #x66 #x0F #x5B)
	  (cvtps2pd #x0F #x5A)
	  (cvtps2pi #x0F #x2D)
	  (cvtsd2si #xF2 #x0F #x2D)
	  (cvtsd2ss #xF2 #x0F #x5A)
	  (cvtsi2sd #xF2 #x0F #x2A)
	  (cvtsi2ss #xF3 #x0F #x2A)
	  (cvtss2sd #xF3 #x0F #x5A)
	  (cvtss2si #xF3 #x0F #x2D)
	  (cvttpd2dq #x66 #x0F #xE6)
	  (cvttpd2pi #x66 #x0F #x2C)
	  (cvttps2dq #xF3 #x0F #x5B)
	  (cvttps2pi #x0F #x2C)
	  (cvttsd2si #xF2 #x0F #x2C)
	  (cvttss2si #xF3 #x0F #x2C)

	  ;; misc
	  (lddqu #xF2 #x0F #xF0)
	  (maskmovdqu #x66 #x0F #xF7)
	  (movddup #xF2 #x0F #x12)
	  (movhlps #x0F #x12)
	  (movlhps #x0F #x16)
	  (movmskpd #x66 #x0F #x50)
	  (movmskps #x0F #x50)
	  (movntdq #x66 #x0F #XE7)
	  (movntpd #x66 #x0F #x2B)
	  (movntps #x0F #x2B)
	  (movshdup #xF3 #x0F #x16)
	  (movsldup #xF3 #x0F #x12)
	  )
	do
	(format stream "~S~%~%"
		`(define-instruction ,(intern (symbol-name inst)) (segment dst src)
		  (:emitter
		   ,@(emit-ops ops)
		   (emit-ea segment src (reg-tn-encoding dst))))))


  ;; INSTRUCTIONS WITH /r IB8
  (loop for (inst . ops) in 
	'(
	  (pextrw #X66 #x0F #xC5)
	  (pinsrw #x66 #x0F #xC4)

	  (pshufd #x66 #x0F #x70)
	  (pshufhw #xF3 #x0F #x70)
	  (pshuflw #xF2 #x0F #x70)

	  (shufpd #x66 #x0F #xC6)
	  (shufps #x0F #xC6)
	  
	  )
	do
	(format stream "~S~%~%"
		`(define-instruction ,(intern (symbol-name inst)) (segment dst src byte)
		  (:emitter
		   ,@(emit-ops ops)
		   (emit-ea segment src (reg-tn-encoding dst))
		   (emit-sized-immediate segment :byte byte)
		   ))))

  ;; COMPARE
  (loop for (inst . ops) in 
	'(
	  (cmppd #x66 #x0F #xC2)
	  (cmpps #x0F #xC2)
	  (cmpsd #xF2 #x0F #xC2)
	  (cmpss #xF3 #x0F #xC2)
	  )
	do
	(format stream "~S~%~%"
		`(define-instruction ,(intern (symbol-name inst)) (segment dst src cond)
		  (:emitter
		   ,@(emit-ops ops)
		   (emit-ea segment src (reg-tn-encoding dst))
		   (emit-sized-immediate segment :byte (cdr (assoc cond
								   '((:eq . #b000) (:e . #b000) (:z . #b000)
								     (:l . #b001) (:nge . #b001)
								     (:le . #b010) (:ng . #b010)
								     (:unord . #b011)
								     (:ne . #b100) (:nz . #b100)
								     (:nl . #b101) (:ge . #b101)
								     (:nle . #b110) (:g . #b110)
								     (:ord . #b111)
								     ))))
		   ))))

  ;; MOVES
  (loop for (inst ops-m2r ops-r2m) in
	'(
	  (movapd (#x66 #x0F #x28) (#x66 #x0F #x29))
	  (movaps (#x0F #x28) (#x0F #x29))

	  (movd (#x66 #x0F #x6E) (#x66 #x0F #x7E))

	  (movdqa (#x66 #x0F #x6F) (#x66 #x0F #x7F))
	  (movdqu (#xF3 #x0F #x6F) (#xF3 #x0F #x7F))

	  (movhpd (#x66 #x0F #x16) (#x66 #x0F #x17))
	  (movhps (#x0F #x16) (#x0F #x17))

	  (movlpd (#x66 #x0F #x12) (#x66 #x0F #x13))
	  (movlps (#x0F #x12) (#x0F #x13))

	  (movq (#xF3 #x0F #x7E) (#x66 #x0F #xD6))

	  (movsd (#xF2 #x0F #x10) (#xF2 #x0F #x11))

	  (movss (#xF3 #x0F #x10) (#xF3 #x0F #x11))

	  (movupd (#x66 #x0F #x10) (#x66 #x0F #x11))
	  (movups (#x0F #x10) (#x0F #x11))
	  )
	do
	(format stream "~S~%~%"
		`(define-instruction ,(intern (symbol-name inst)) (segment dst src)
		  (:emitter
		   (cond ((xmm-register-p dst)
			  ,@(emit-ops ops-m2r)
			  (emit-ea segment src (reg-tn-encoding dst)))
			 (t ,@(emit-ops ops-r2m)
			    (emit-ea segment dst (reg-tn-encoding src))))))))

  ;; misc
  (loop for (name mode . opcodes) in
	'(
	  (pslld-ib 6 #x66 #x0F #x72)
	  (pslldq-ib 7 #x66 #x0F #x73)
	  (psllq-ib 6 #x66 #x0F #x73)
	  (psllw-ib 6 #x66 #x0F #x71)
	  (psrad-ib 4 #x66 #x0F #x72)
	  (psraw-ib 4 #x66 #x0F #x71)
	  (psrld-ib 2 #x66 #x0F #x72)
	  (psrldq-ib 3 #x66 #x0F #x73)	  
	  (psrlq-ib 2 #x66 #x0F #x73)
	  (psrlw-ib 2 #x66 #x0F #x71)
	  )
	do
	(format stream "~S~%~%"
		`(define-instruction ,(intern (symbol-name name)) (segment dst amount)
		  (:emitter
		   ,@(emit-ops opcodes)
		   (emit-ea segment dst ,mode)
		   (emit-byte segment amount)))))

  )

(defun gen-ops-to-file (filename)
  (with-open-file (stream filename :direction :output :if-exists :supersede)
    (gen-ops stream)))