#|
Copyright (c) 2005 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(in-package :cl-user)
;;(declaim (optimize (speed 3) (space 0) (debug 0) (safety 0)))

(defmacro make-vector ()
  `(make-array 4 :element-type 'single-float :initial-element 0f0 :adjustable nil :fill-pointer nil))

(defmacro make-scalar ()
  `(make-array 1 :element-type 'single-float :initial-element 0f0 :adjustable nil :fill-pointer nil))

(declaim
 (ftype (function ((simple-array single-float (4)) single-float) (simple-array single-float (4))) v* v2*)
 (ftype (function ((simple-array single-float (4)) (simple-array single-float (4))) (simple-array single-float (4))) v+ v- v2+ v2-)
 (ftype (function ((simple-array single-float (4)) (simple-array single-float (4))) single-float) dot dot2)
 (ftype (function ((simple-array single-float (4))) (simple-array single-float (4))) unitise unitise2)
 (ftype (function (single-float single-float single-float) (simple-array single-float (4))) vec)
 )

(declaim (inline v* v+ v- dot unitise vec v2* v2+ v2- dot2 unitise2))

(defun v2* (a s)
  (let ((res (make-vector)))
    (declare (type (simple-array single-float (4)) a res) (type single-float s))
    (loop for i from 0 to 3 do (setf (aref res i) (* (aref a i) s)))
    res))

(defun v* (a s)
  (let ((res (make-vector)))
    (sb-sys:%primitive sb-vm::%sse-vect-scalar-mul/single-float res a s)
  res))

(defun v2+ (a b)
  (let ((res (make-vector)))
    (declare (type (simple-array single-float (4)) a b res))
    (loop for i from 0 to 3 do (setf (aref res i) (+ (aref a i) (aref b i))))
    res))

(defun v+ (a b)
  (let ((res (make-vector)))
    (sb-sys:%primitive sb-vm::%sse-vect-add/single-float res a b)
    res))

(defun v2- (a b)
  (let ((res (make-vector)))
    (declare (type (simple-array single-float (4)) a b res))
    (loop for i from 0 to 3 do (setf (aref res i) (- (aref a i) (aref b i))))
    res))

(defun v- (a b)
  (let ((res (make-vector)))
    (sb-sys:%primitive sb-vm::%sse-vect-sub/single-float res a b)
    res))

(defun dot2 (a b)
  (declare (type (simple-array single-float (4)) a b))
  (loop for i from 0 to 3 sum (* (aref a i) (aref b i)) into res finally (return res)))

(defun dot (a b)
  (let ((res (make-scalar)))
    (sb-sys:%primitive sb-vm::%sse-vect-dot/single-float res a b)
    (aref res 0)))

(defun unitise2 (a)
  (v2* a (/ 1f0 (sqrt (dot2 a a)))))
  
(defun unitise (a)
  (let ((res (make-vector)))
    (sb-sys:%primitive sb-vm::%sse-vect-normalize/single-float res a)
    res))

(defun vec (x y z)
  (let ((res (make-vector)))
    (setf (aref res 0) x (aref res 1) y (aref res 2) z)
    res))

(defun test-foo2 ()
  (let* ((v    (v- (vec 10f0 10f0 0f0) (vec 3f0 3f0 1f0)))
         (b    (dot v (vec 0f0 0f0 10f0)))
         (disc (+ (- (* b b) (dot v v)) (* 1.5 1.5))))
    disc))

(defun test-bar4 ()
;;  (let ((x (vec (random 1f6) (random 1f6) (random 1f6)))
;;	(y (vec (random 1f6) (random 1f6) (random 1f6)))
;;	(z (vec (random 1f6) (random 1f6) (random 1f6)))
;;	(idx 0)
;;	(res (make-vector)))
  (let ((x (Vec 1f0 2f0 3f0))
	(idx 0))

;;    (sb-sys:%primitive sb-vm::%store-xmm-to-array/single-float res 0
;;		       (sb-sys:%primitive sb-vm::%sse-vect-add2/single-float 
;;    (the xmm (sb-sys:%primitive sb-vm::data-vector-ref/simple-array-single-float/xmm x idx))
;;					  (sb-sys:%primitive sb-vm::data-vector-ref/simple-array-single-float/xmm y idx))

;;					  (data-vector-ref x 0)
;;					  (data-vector-ref y 0))
;;    (sb-sys:%primitive sb-vm::%store-xmm-to-array/single-float y 0
;;    (the xmm (sb-sys:%primitive sb-vm::%load-xmm-from-array/single-float x 0))
;;					  (sb-sys:%primitive sb-vm::%load-xmm-from-array/single-float y 0)))
    (the single-float (sb-sys:%primitive sb-vm::data-vector-ref/simple-array-single-float x idx))

;;    (sb-sys:%primitive sb-vm::move-from-xmm
;;		       (sb-sys:%primitive sb-vm::myvop4 x))))

))
(defun test-bar3 (x y)
  (v- (v+ x y) (unitise y)))

(defun test-bar ()
  (let ((x (vec (random 1f6) (random 1f6) (random 1f6)))
	(y (vec (random 1f6) (random 1f6) (random 1f6)))
	res)
    (time (dotimes (i 1000000)
	    (setf res (dot (v- (v+ x y) y) (unitise y)))))
    (time (dotimes (i 1000000)
	    (setf res (dot2 (v2- (v2+ x y) y) (unitise2 y)))))
    res))



(defun test-foo ()
  (format t "~S.~%"  (unitise (vec -1.0 -3.0 2.0))))

(defun test-vector ()
  (let ((vec1 (make-vector))
	(vec2 (make-vector))
	(vec3 (make-vector))
	(temp (make-array 1 :element-type 'single-float :initial-element 0f0))
	res)

    (loop for i of-type fixnum from 0 below 3 
	  do (setf (aref vec1 i) (float (random 1f6))
		   (aref vec2 i) (float (random 1f6))))


    (format t "Data: ~S~%~S~%" vec1 vec2)

    (sb-sys:%primitive sb-vm::%sse-vect-add/single-float vec3 vec1 vec2)
    (format t "Add: ~S, ok? ~A~%" vec3
	    (loop for equal = t 
		  for res-elt across res 
		  for idx from 0 
		  for ok-elt = (+ (aref vec1 idx) (aref vec2 idx))
		  when (/= ok-elt res-elt) do (setq equal nil)
		  finally (return equal)))

    (sb-sys:%primitive sb-vm::%sse-vect-normalize/single-float vec3 vec1)
    (sb-sys:%primitive sb-vm::%sse-vect-len/single-float temp vec3)
    (format t "Normalize 1: ~S, len ~S.~%" vec3 temp)

    (sb-sys:%primitive sb-vm::%sse-vect-normalize/single-float vec3 vec2)
    (sb-sys:%primitive sb-vm::%sse-vect-len/single-float temp vec3)
    (format t "Normalize 2: ~S, len ~S.~%" vec3 temp)
    
    (sb-sys:%primitive sb-vm::%sse-vect-dot/single-float temp vec1 vec2)
    (format t "Dot: ~S, ok? ~A.~%" temp
	    (loop for a across vec1 
		  for b across vec2
		  sum (* a b) into res
		  finally (return (= res (aref temp 0)))))

    ))


