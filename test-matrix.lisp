#|
Copyright (c) 2005 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(in-package :cl-user)

(declaim (optimize (speed 3) (space 0) (safety 0) (debug 0)))


(defun test-matrix (&optional (test-count 10000000))
  (let ((mat1 (make-array 9 :element-type 'single-float :initial-element 0f0))
	(mat2 (make-array 9 :element-type 'single-float :initial-element 0f0))
	(naive #())
	(sse #()) 
	)
    (declare (type (simple-vector) naive sse) (type fixnum test-count))

    (loop for i of-type fixnum from 0 below 9 do (setf (aref mat1 i) (float (random 1f6))
					(aref mat2 i) (float (random 1f6))))

    (format t "Data: ~S~%~S~%" mat1 mat2)

    (setf naive (naive-mul33 mat1 mat2)
	  sse (sse-mul33 mat1 mat2))

    (format t "naive mul: ~S~%" naive)
    (format t "sse mul: ~S~%" sse)
    (format t "EQUALP? ~A~%" (loop for equal = t 
				   for n of-type single-float across naive
				   for s of-type single-float across sse
				   when (/= n s) do (setq equal nil) 
				   finally (return equal)))
    (format t "naive, ~D ops ~%" test-count)
    (time-sample-form #'(lambda ()
			  (dotimes (i test-count) 
			    (setf naive (naive-mul33 mat1 mat2)))))
    
    (format t "sse, ~D ops ~%" test-count)
    (time-sample-form #'(lambda ()
			  (dotimes (i test-count) 
			    (setf sse (sse-mul33 mat1 mat2)))))
    
    ))

(defun sse-mul33 (mat1 mat2)
  (let ((res (make-array 9 :element-type 'single-float :initial-element 0f0)))
    (declare (type (simple-array single-float (9)) mat1 mat2 res))
    (sb-sys:%primitive sb-vm::%sse-matrix-mul-3x3/single-float res mat1 mat2)
    res))

(defun naive-mul33 (mat1 mat2)
  (let ((res (make-array 9 :element-type 'single-float :initial-element 0f0)))
    (declare (type (simple-array single-float (9)) mat1 mat2 res))
    (loop for row of-type fixnum from 0 to 2 do
	  (loop for col of-type fixnum from 0 to 2 do
		(loop for elt of-type fixnum from 0 to 2 do
		      (incf (aref res (+ (* row 3) col))
			    (* (aref mat1 (+ (* row 3) elt)) (aref mat2 (+ (* elt 3) col)))))))
    res))


