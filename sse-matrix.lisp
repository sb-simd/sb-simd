#|
Copyright (c) 2005 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
#|

http://developer.intel.com/design/pentiumiii/sml/24504501.pdf

|#
(in-package :sb-vm)

(defmacro vect-ea (base &optional idx) 
  (let ((disp
	 (if (and idx (numberp idx))
	     `(+ (- (* VECTOR-DATA-OFFSET N-WORD-BYTES) OTHER-POINTER-LOWTAG) ,idx)
	     `(- (* VECTOR-DATA-OFFSET N-WORD-BYTES) OTHER-POINTER-LOWTAG))))

;;    (format t "ea ~A ~A ~A~%" base idx (and idx (symbolp idx)))
    (if (and idx (symbolp idx))
	`(make-ea :dword :base ,base :index ,idx :disp ,disp)
	`(make-ea :dword :base ,base :disp ,disp))))

(DEFINE-VOP (%sse-matrix-mul-3x3/single-float)
    (:POLICY :FAST-SAFE)
  (:ARGS (RESULT :SCS (DESCRIPTOR-REG))
	 (MAT1 :SCS (DESCRIPTOR-REG))
	 (MAT2 :SCS (DESCRIPTOR-REG)))
  (:ARG-TYPES SIMPLE-ARRAY-SINGLE-FLOAT
	      SIMPLE-ARRAY-SINGLE-FLOAT
	      SIMPLE-ARRAY-SINGLE-FLOAT)

  (:TEMPORARY (:SC XMM-REG) X0)
  (:TEMPORARY (:SC XMM-REG) X1)
  (:TEMPORARY (:SC XMM-REG) X2)
  (:TEMPORARY (:SC XMM-REG) X3)
  (:TEMPORARY (:SC XMM-REG) X4)
  (:TEMPORARY (:SC XMM-REG) X5)
  (:TEMPORARY (:SC XMM-REG) X6)
  (:TEMPORARY (:SC XMM-REG) X7)

  (:GENERATOR 10
		(inst movss x2 (vect-ea mat2 32))
		(inst movhps x2 (vect-ea mat2 24))
			
		(inst movss x3 (vect-ea mat1))
		(inst movss x4 (vect-ea mat1 4))

		(inst movss x0 (vect-ea mat2))
		(inst movhps x0 (vect-ea mat2 4))
		(inst shufps x2 x2 #X36)
		(inst shufps x3 x3 0)

		(inst movss x1 (vect-ea mat2 12))
		(inst movhps x1 (vect-ea mat2 16))

		(inst shufps x4 x4 0)
		(inst mulps x3 x0)
		(inst movss x5 (vect-ea mat1 8))
		(inst movss x6 (vect-ea mat1 12))
		(inst mulps x4 x1)
		(inst shufps x5 x5 0)
		(inst mulps x5 x2)
		(inst shufps x6 x6 0)
		(inst mulps x6 x0)
		(inst addps x3 x4)

		(inst movss x7 (vect-ea mat1 16))
		(inst movss x4 (vect-ea mat1 28))

		(inst shufps x7 x7 0)
		(inst addps x3 x5)
		(inst mulps x7 x1)

		(inst shufps x4 x4 0)
			
		(inst movss x5 (vect-ea mat1 20))
		(inst shufps x5 x5 0)
		(inst mulps x4 x1)

		(inst mulps x5 x2)
		(inst addps x6 x7)

		(inst movss x1 (vect-ea mat1 24))
			
		(inst movss (Vect-ea result) x3)
		(inst movhpd (vect-ea result 4) x3)

		(inst addps x6 x5)
		(inst shufps x1 x1 0)

		(inst movss x5 (vect-ea mat1 32))
		(inst mulps x1 x0)
		(inst shufps x5 x5 0)

		(inst movss (vect-ea result 12) x6)
		(inst mulps x5 x2)
		(inst addps x1 x4)
		(inst movhps (vect-ea result 16) x6)
		(inst addps x1 x5)
		(inst shufps x1 x1 #x8F)

		(inst movhps (vect-ea result 24) x1)
		(inst movss (vect-ea result 32) x1)
		))
			

			
