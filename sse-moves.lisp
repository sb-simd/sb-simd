(in-package :sb-vm)

(defun ea-for-xmm-desc (tn)
  (make-ea :xmmword :base tn 
	   :disp (- (* xmm-value-slot n-word-bytes) other-pointer-lowtag)))

(defun ea-for-xmm-stack (tn)
  (make-ea :xmmword :base ebp-tn 
	   :disp (- (* (+ (tn-offset tn)
			  4)
		       n-word-bytes))))

(define-move-fun (load-xmm 2) (vop x y)
  ((xmm-stack) (xmm-reg))
  (inst movdqu y (ea-for-xmm-stack x)))

(define-move-fun (store-xmm 2) (vop x y)
  ((xmm-reg) (xmm-stack))
  (inst movdqu (ea-for-xmm-stack y) x))

(define-move-fun (load-xmm-single 2) (vop x y)
  ((single-stack) (xmm-reg))
  (inst movss y (ea-for-sf-stack x)))

(define-move-fun (store-xmm-single 2) (vop x y)
  ((xmm-reg) (single-stack))
  (inst movss (ea-for-sf-stack y) x))


(define-vop (data-vector-ref/simple-array-single-float/xmm)
  (:note "array to xmm access")
  (:translate data-vector-ref)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (index :scs (any-reg)))
  (:arg-types simple-array-single-float positive-fixnum)
  (:results (value :scs (xmm-reg)))
  (:result-types xmm)
  (:generator 5
	      (inst movdqu value
		    (make-ea :xmmword :base object :index index :scale 1
			     :disp (- (* vector-data-offset
					 n-word-bytes)
				      other-pointer-lowtag)))))

(define-vop (data-vector-ref-c/simple-array-single-float/xmm)
  (:note "array to xmm access")
  (:translate data-vector-ref)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg)))
  (:info index)
  (:arg-types simple-array-single-float (:constant (signed-byte 30)))
  (:results (value :scs (xmm-reg)))
  (:result-types xmm)
  (:generator 4
	      (inst movdqu value (make-ea :xmmword :base object
					  :disp (- (+ (* vector-data-offset
							 n-word-bytes)
						      (* 16 index))
						   other-pointer-lowtag)))))



(define-vop (data-vector-set/simple-array-single-float/xmm)
  (:note "inline array store")
  (:translate data-vector-set)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (index :scs (any-reg))
         (value :scs (xmm-reg) :target result))
  (:arg-types simple-array-single-float positive-fixnum xmm)
  (:results (result :scs (xmm-reg)))
  (:result-types xmm)
  (:generator 5
	      (inst movdqu (make-ea :dword :base object :index index :scale 1
				    :disp (- (* vector-data-offset
						n-word-bytes)
					     other-pointer-lowtag))
		    value)
	      (unless (location= value result)
		(move result value))))

(define-vop (data-vector-set-c/simple-array-single-float/xmm)
  (:note "inline array store")
  (:translate data-vector-set)
  (:policy :fast-safe)
  (:args (object :scs (descriptor-reg))
         (value :scs (xmm-reg) :target result))
  (:info index)
  (:arg-types simple-array-single-float (:constant (signed-byte 30))
              xmm)
  (:results (result :scs (xmm-reg)))
  (:result-types xmm)
  (:generator 4
	      (inst movdqu (make-ea :dword :base object
				    :disp (- (+ (* vector-data-offset
						   n-word-bytes)
						(* 16 index))
					     other-pointer-lowtag))
		    value)
	      (unless (location= value result)
		(move result value))))


(define-vop (myvop1)
    (:policy :fast-safe)
  (:args (src :scs (descriptor-reg)))
  (:arg-types simple-array-single-float)
  (:results (dest :scs (descriptor-reg)))
  (:result-types fixnum)
  (:generator 1
	      (inst mov dest (fixnumize 100))))

(define-vop (myvop2)
    (:policy :fast-safe)
  (:args (src :scs (descriptor-reg)))
  (:arg-types simple-array-single-float)
  (:results (dest :scs (descriptor-reg)))
  (:result-types fixnum)
  (:temporary (:scs (xmm-reg)) x0)
  (:generator 1
	      (inst movdqu x0 (make-ea :xmmword :base src :disp (- (* vector-data-offset n-word-bytes) other-pointer-lowtag)))
	      (inst mov dest (fixnumize 100))))

(define-vop (myvop3)
    (:policy :fast-safe)
  (:args (src :scs (descriptor-reg)))
  (:arg-types simple-array-single-float)
  (:results (dest :scs (xmm-reg)))
  (:result-types xmm)
  (:temporary (:scs (xmm-reg) :to :result) x0)
  (:generator 1
	      (inst movdqu x0 (make-ea :xmmword :base src :disp (- (* vector-data-offset n-word-bytes) other-pointer-lowtag)))
	      (move dest x0)))

(define-vop (myvop4)
    (:policy :fast-safe)
  (:args (src :scs (descriptor-reg)))
  (:arg-types simple-array-single-float)
  (:results (dest :scs (single-reg)))
  (:result-types single-float)
  (:temporary (:scs (single-reg) :to :result) x0)
  (:generator 1
;;	      (move x0 (make-ea :dword :base src :disp (- (* vector-data-offset n-word-bytes) other-pointer-lowtag)))
;;	      (move dest x0)))
	      (inst nop)))


(define-vop (%load-xmm-from-array/single-float)
    (:policy :fast-safe)
    (:args (src :scs (descriptor-reg))
	   (index :scs (unsigned-reg)))
    (:ARG-TYPES SIMPLE-ARRAY-SINGLE-FLOAT fixnum)
    (:results (dest :scs (xmm-reg)))
    (:result-types xmm)
    (:generator 1
		(inst shl index 2)
		(inst movdqu dest (make-ea :xmmword :base src :index index
					   :disp (- (* VECTOR-DATA-OFFSET N-WORD-BYTES) OTHER-POINTER-LOWTAG)))))

    
(define-vop (%store-xmm-to-array/single-float)
    (:policy :fast-safe)
    (:args (dest :scs (descriptor-reg))
	   (index :scs (unsigned-reg))
	   (src :scs (xmm-reg)))
    (:ARG-TYPES SIMPLE-ARRAY-SINGLE-FLOAT fixnum XMM)
    (:generator 1
		(inst shl index 2)
		(inst movdqu (make-ea :xmmword :base dest :index index 
				      :disp (- (* VECTOR-DATA-OFFSET N-WORD-BYTES) OTHER-POINTER-LOWTAG))
		      src)))

    
(define-vop (xmm-move)
  (:args (x :scs (xmm-reg) :target y :load-if (not (location= x y))))
  (:results (y :scs (xmm-reg) :load-if (not (location= x y))))
  (:note "xmm move")
  (:generator 0
	      (unless (location= x y)
		(inst movdqa y x))))

(define-move-vop xmm-move :move (xmm-reg) (xmm-reg))

(define-vop (move-from-xmm)
  (:args (x :scs (xmm-reg) :to :save))
  (:results (y :scs (descriptor-reg)))
  (:node-var node)
  (:note "xmm to pointer coercion")
  (:generator 13
     (with-fixed-allocation (y
                             xmm-widetag
                             xmm-size node)
       (inst movdqu (ea-for-xmm-desc y) x))))

(define-move-vop move-from-xmm :move (xmm-reg) (descriptor-reg))

(define-vop (move-to-xmm)
  (:args (x :scs (descriptor-reg)))
  (:results (y :scs (xmm-reg)))
  (:note "pointer to xmm coercion")
  (:generator 2
	      (inst movdqu y (ea-for-xmm-desc x))))

(define-move-vop move-to-xmm :move (descriptor-reg) (xmm-reg))


(define-vop (move-xmm-arg)
    (:args (x :scs (xmm-reg) :target y)
	   (fp :scs (any-reg)
	       :load-if (not (sc-is y xmm-reg))))
  (:results (y))
  (:note "xmm argument move")
  (:generator 6
	      (sc-case y
		       (xmm-reg
			(unless (location= x y)
			  (inst movdqa y x)))

		       (xmm-stack
			(if (= (tn-offset fp) esp-offset)
			    (let* ((offset (* (tn-offset y) n-word-bytes))
				   (ea (make-ea :xmmword :base fp :disp offset)))
			      (inst movdqu ea x))

			    (let ((ea (make-ea :xmmword :base fp
					       :disp (- (* (+ (tn-offset y) 4)
							   n-word-bytes)))))
			      (inst movdqu ea x)))))))

(define-move-vop move-xmm-arg :move-arg (xmm-reg descriptor-reg) (xmm-reg))

(define-move-vop move-arg :move-arg (xmm-reg) (descriptor-reg))



