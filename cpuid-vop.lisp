#|
Copyright (c) 2005 Risto Laakso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
|#
(in-package :sb-c)
(ignore-errors (defknown cl-user::%read-cpu (unsigned-byte-32 simple-array-unsigned-byte-16) nil))

(in-package :sb-vm)

(defmacro vect-ea (vect idx)
  `(make-ea :dword :base ,vect :index ,idx
    :disp (- (* vector-data-offset n-word-bytes) other-pointer-lowtag)))

(define-vop (%read-cpu/x86)
    (:translate cl-user::%read-cpu)
  (:policy :fast)

  (:args (n :scs (unsigned-reg) :target eax)
	 (result :scs (descriptor-reg)))
  (:arg-types unsigned-byte-32 simple-array-unsigned-byte-16)

  (:temporary (:sc unsigned-reg :offset eax-offset :from (:argument 0)) eax)
  (:temporary (:sc unsigned-reg :offset ebx-offset) ebx)
  (:temporary (:sc unsigned-reg :offset ecx-offset) ecx)
  (:temporary (:sc unsigned-reg :offset edx-offset) edx)
  (:temporary (:sc unsigned-reg) index)

  (:generator 10

	      (inst xor index index)

	      (inst mov eax n)

	      ;; zero regs
	      (inst xor ebx ebx)
	      (inst xor ecx ecx)
	      (inst xor edx edx)

	      ;; cpuid
	      (inst cpuid)

	      (inst push edx)

	      ;; EAX
	      (inst mov edx eax)
	      (inst shr edx 16)
	      (inst and edx #xFFFF)
	      (inst mov (vect-ea result index) edx)
	      (inst add index 2)

	      (inst mov edx eax)
	      (inst and edx #xFFFF)
	      (inst mov (vect-ea result index) edx)
	      (inst add index 2)

	      (inst pop edx)

	      ;; EBX

	      (inst mov eax ebx)
	      (inst shr eax 16)
	      (inst and eax #xFFFF)
	      (inst mov (vect-ea result index) eax)
	      (inst add index 2)

	      (inst mov eax ebx)
	      (inst and eax #xFFFF)
	      (inst mov (vect-ea result index) eax)
	      (inst add index 2)

	      ;; ECX

	      (inst mov eax ecx)
	      (inst shr eax 16)
	      (inst and eax #xFFFF)
	      (inst mov (vect-ea result index) eax)
	      (inst add index 2)

	      (inst mov eax ecx)
	      (inst and eax #xFFFF)
	      (inst mov (vect-ea result index) eax)
	      (inst add index 2)

	      ;; EDX
	      (inst mov eax edx)
	      (inst shr eax 16)
	      (inst and eax #xFFFF)
	      (inst mov (vect-ea result index) eax)
	      (inst add index 2)

	      (inst mov eax edx)
	      (inst and eax #xFFFF)
	      (inst mov (vect-ea result index) eax)
	      (inst add index 2)
	      ))





